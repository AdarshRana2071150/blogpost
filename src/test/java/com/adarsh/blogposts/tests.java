package com.adarsh.blogposts;

import com.adarsh.blogposts.repository.PostRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Rollback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
public class tests {
    private final PostRepository postRepository;

    @Autowired
    public tests(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

//    @Test
//    public void testSearch() throws ParseException {
//        System.out.println(postRepository.findBySearch("Adarsh", new SimpleDateFormat("yyyy-MM-dd").parse("2022-03-24"), List.of(53), PageRequest.of(0, 10)).getContent());
//    }
}
