package com.adarsh.blogposts.service;

import com.adarsh.blogposts.entity.Role;
import com.adarsh.blogposts.entity.User;
import com.adarsh.blogposts.repository.RoleRepository;
import com.adarsh.blogposts.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {

        User user = userRepository.findByName(name);
        if(user == null){
            throw new UsernameNotFoundException("user 404");
        }

        return new UserPrincipal(user);
    }

    public User findByName(String name) {
        return userRepository.findByName(name);
    }

    public void save(User user, String authority) {
        Role role = roleRepository.findByName(authority);
        System.out.println(role);
        Set<Role> setOfRoles = new HashSet<>();
        setOfRoles.add(role);
        user.setRoles(setOfRoles);
        userRepository.save(user);
    }
}
