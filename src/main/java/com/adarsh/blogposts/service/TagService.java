package com.adarsh.blogposts.service;

import com.adarsh.blogposts.entity.Tag;
import com.adarsh.blogposts.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {
    @Autowired
    private TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public List<Tag> findAllByNameIgnoreCase(String tag) {
        return tagRepository.findAllByNameIgnoreCase(tag);
    }

    public void save(Tag createTag) {
        tagRepository.save(createTag);
    }

    public List<Tag> findAll() {
       return tagRepository.findAll();
    }

    public Tag findTagByName(String tag) {
        return tagRepository.findByName(tag);
    }
}
