package com.adarsh.blogposts.service;

import com.adarsh.blogposts.entity.Comment;
import com.adarsh.blogposts.entity.Post;
import com.adarsh.blogposts.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {
    private CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository){
        this.commentRepository = commentRepository;
    }


    public void saveComment(Comment comment) {
        commentRepository.save(comment);
    }

    public List<Comment> getAllCommentsByPostId(Post post) {
        List<Comment> list = commentRepository.getByPostId(post);
        return list;
    }

    public Comment getCommentById(Integer id){
        return commentRepository.getById(id);
    }

    public void deleteComment(Comment comment) {
        commentRepository.deleteByIdAndPostId(comment.getId(),comment.getPostId());
    }
}
