package com.adarsh.blogposts.service;

import com.adarsh.blogposts.entity.Post;
import com.adarsh.blogposts.entity.Tag;
import com.adarsh.blogposts.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PostService {
    @Autowired
    private PostRepository postRepository;

    public PostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public Optional<Post> getPost(Integer id) {
        return postRepository.findById(id);
    }


    public Page<Post> getAllPosts(Integer page) {
        Pageable firstPageWithTenElements = PageRequest.of(page-1, 10);
        return postRepository.findAll(firstPageWithTenElements);
    }

    public void save(Post post) {
        postRepository.save(post);
    }

    public void deleteById(Integer id) {
        postRepository.deleteById(id);
    }

    public Page<Post> sortPost(String orderBy, Integer page){
        Pageable firstPageWithTenElements = PageRequest.of(page, 10);
        if(orderBy.equals("DESC")){
            return  postRepository.findAllByOrderByPublishedAtDesc(firstPageWithTenElements);
        }
        return  postRepository.findAllByOrderByPublishedAtAsc(firstPageWithTenElements);
    }

    public Page<Post> findAllUsingProcess(String search, String fromDate, String toDate, List<Tag> allTags, String orderBy) throws ParseException {
        Pageable firstPageWithTenElements = PageRequest.of(0, 10);
//        System.out.println(search);
//        System.out.println(toDate);
//        System.out.println(fromDate);
//        System.out.println(orderBy);
//        System.out.println(orderBy.getClass());
        if(search.equals("") && toDate.equals("") && fromDate.equals("") && orderBy.equals("none")){
            return postRepository.findAll(firstPageWithTenElements);
        }
        System.out.println(search);
        List<Integer> allTheTags = new ArrayList<>();
        for (Tag tag : allTags) {
            if(tag == null){
                break;
            }
            allTheTags.add(tag.getId());
        }
        fromDate = Objects.equals(fromDate, "") ?"2022-03-26":fromDate;
        toDate = Objects.equals(toDate, "") ? "2022-03-29":toDate;
        Page<Post> page = null;
        if(orderBy.equals("DESC")) {
            page = postRepository.findBySearchOrderByDESC(search.toLowerCase(), new SimpleDateFormat("yyyy-MM-dd").parse(fromDate), new SimpleDateFormat("yyyy-MM-dd").parse(toDate), allTheTags, firstPageWithTenElements);
        } else {
            page = postRepository.findBySearchOrderByASC(search.toLowerCase(), new SimpleDateFormat("yyyy-MM-dd").parse(fromDate), new SimpleDateFormat("yyyy-MM-dd").parse(toDate), allTheTags, firstPageWithTenElements);
        }
        System.out.println(page.getContent());
        return page;
    }
}
