package com.adarsh.blogposts.entity;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class ProcessData {
    private String search;
    private String date;
    private String allTags;
    private String orderBy;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAllTags() {
        return allTags;
    }

    public void setAllTags(String allTags) {
        this.allTags = allTags;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public String toString() {
        return "Process{" +
                "search='" + search + '\'' +
                ", date=" + date +
                ", allTags=" + allTags +
                ", orderBy='" + orderBy + '\'' +
                '}';
    }

    public ProcessData(String search, String date, String allTags, String orderBy) {
        this.search = search;
        this.date = date;
        this.allTags = allTags;
        this.orderBy = orderBy;
    }

    public ProcessData() {
    }
}
