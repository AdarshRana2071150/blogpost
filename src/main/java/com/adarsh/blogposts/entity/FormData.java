package com.adarsh.blogposts.entity;

import java.util.List;

public class FormData {
    private Integer id;
    private String title;
    private String author;
    private String content;
    private List<String> tags;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }


    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public FormData(String title, String author, String content, List<String> tags) {
        this.title = title;
        this.author = author;
        this.content = content;
        this.tags = tags;
    }

    public FormData() {
    }

    @Override
    public String toString() {
        return "FormData{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", content='" + content + '\'' +
                ", tags=" + tags +
                '}';
    }
}
