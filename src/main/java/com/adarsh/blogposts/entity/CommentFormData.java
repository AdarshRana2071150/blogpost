package com.adarsh.blogposts.entity;

import java.time.LocalDateTime;

public class CommentFormData {
    private Integer id;
    private String name;
    private String comment;
    private String email;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private Post postId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Post getPostId() {
        return postId;
    }

    public void setPostId(Post postId) {
        this.postId = postId;
    }

    public CommentFormData(String name, String comment, String email, LocalDateTime createdAt, LocalDateTime updatedAt, Post postId) {
        this.name = name;
        this.comment = comment;
        this.email = email;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.postId = postId;
    }

    public CommentFormData() {
    }

    @Override
    public String toString() {
        return "CommentFormData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", comment='" + comment + '\'' +
                ", email='" + email + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", postId=" + postId +
                '}';
    }
}
