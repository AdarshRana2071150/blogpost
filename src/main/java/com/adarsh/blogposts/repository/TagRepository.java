package com.adarsh.blogposts.repository;

import com.adarsh.blogposts.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, Integer> {
    List<Tag> findAllByNameIgnoreCase(String tag);
    Tag findByName(String tag);

}
