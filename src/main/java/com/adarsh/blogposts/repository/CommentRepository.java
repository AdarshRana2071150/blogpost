package com.adarsh.blogposts.repository;

import com.adarsh.blogposts.entity.Comment;
import com.adarsh.blogposts.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
    List<Comment> getByPostId(Post postId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Comment comment WHERE comment.id = :id AND comment.postId = :postId")
    void deleteByIdAndPostId(@Param("id") Integer id,
                             @Param("postId") Post PostId);
}
