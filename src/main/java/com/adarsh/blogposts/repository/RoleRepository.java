package com.adarsh.blogposts.repository;

import com.adarsh.blogposts.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(String authority);
}
