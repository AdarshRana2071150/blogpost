package com.adarsh.blogposts.repository;

import com.adarsh.blogposts.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer>{
    Page<Post> findAllByOrderByPublishedAtAsc(Pageable firstPageWithTenElements);
    Page<Post> findAllByOrderByPublishedAtDesc(Pageable firstPageWithTenElements);

    @Query("SELECT p FROM Post p JOIN p.tags t WHERE " +
            "(lower(p.title) LIKE %:search% OR " +
            "lower(p.author) LIKE %:search% OR " +
            "lower(p.content) LIKE %:search% OR " +
            "lower(t.name) LIKE %:search%) OR" +
            " DATE(p.publishedAt) BETWEEN :date AND :endDate or " +
            " (t.id IN :allTags) ORDER BY p.publishedAt DESC")

    Page<Post> findBySearchOrderByDESC(@Param("search") String search,
                                       @Param("date") Date date,
                                       @Param("endDate") Date endDate,
                                       @Param("allTags") List<Integer> allTags,
                                       Pageable firstPageWithTenElements);

    @Query("SELECT p FROM Post p JOIN p.tags t WHERE " +
            "(lower(p.title) LIKE %:search% OR " +
            "lower(p.author) LIKE %:search% OR " +
            "lower(p.content) LIKE %:search% OR " +
            "lower(t.name) LIKE %:search%) OR" +
            " DATE(p.publishedAt) BETWEEN :date AND :endDate or " +
            " (t.id IN :allTags) ORDER BY p.publishedAt ASC")

    Page<Post> findBySearchOrderByASC(@Param("search") String search,
                                      @Param("date") Date date,
                                      @Param("endDate") Date endDate,
                                      @Param("allTags") List<Integer> allTags,
                                      Pageable firstPageWithTenElements);
}
