package com.adarsh.blogposts.controller;

import com.adarsh.blogposts.entity.*;
import com.adarsh.blogposts.service.CommentService;
import com.adarsh.blogposts.service.PostService;
import com.adarsh.blogposts.service.TagService;
import com.adarsh.blogposts.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.*;

@Controller
public class PostController {
    @Autowired
    private PostService postService;

    @Autowired
    private TagService tagService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private UserDetailsService userDetailsService;

    @GetMapping("/")
    public String loginRedirect(){
        return "redirect:/1";
    }

    @GetMapping("/login")
    public String loginPage(){
        return "login.html";
    }

    @RequestMapping("/logout-success")
    public String logoutPage(){
        return "logout.html";
    }

    @GetMapping("/{page}")
    public String getPosts(@PathVariable("page") Integer page,
                           Model model){
        model.addAttribute("search", null);
        model.addAttribute("orderBy", null);
        model.addAttribute("toDate", null);
        model.addAttribute("fromDate", null);
        return "redirect:/{page}/process?search=&toDate=&fromDate=&orderBy=none";
    }

    @GetMapping("/{page}/process")
    public String getPosts(
                           @RequestParam(name = "search", required = false) String search,
                           @RequestParam(name = "tagsToAdd", required = false) List<String> tagsToAdd,
                           @RequestParam(name = "orderBy", required = false) String orderBy,
                           @RequestParam(name = "fromDate", required = false) String fromDate,
                           @RequestParam(name = "toDate", required = false) String toDate,
                           @PathVariable(name = "page", required = false) Integer page,
                           Model model) throws ParseException {
        List<Tag> tagsToBeAdded = new ArrayList<>();
        if(tagsToAdd != null) {
            for (String tag : tagsToAdd) {
                Tag t = tagService.findTagByName(tag);
                tagsToBeAdded.add(t);
            }
        }
        Page<Post> processedResult = postService.findAllUsingProcess(search, fromDate, toDate, tagsToBeAdded, orderBy);
        Integer totalPages = processedResult.getTotalPages();
        long totalItems = processedResult.getTotalElements();

        List<Tag> allTags = tagService.findAll();
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("totalItems",totalItems);
        model.addAttribute("page",page);
        model.addAttribute("tagsToAdd", tagsToAdd);
        model.addAttribute("allTags", allTags);
        model.addAttribute("listPosts", processedResult.getContent());
        model.addAttribute("search", search);
        model.addAttribute("orderBy",  orderBy);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        return "posts";
    }

    @GetMapping("/view/{id}")
    public String getPost(@PathVariable("id") Integer id, Model model){
        Optional<Post> optionalPosts = postService.getPost(id);
        Post post = optionalPosts.get();
        List<Comment> listComment = commentService.getAllCommentsByPostId(post);
        model.addAttribute("post", post);
        model.addAttribute("listComments", listComment);
        return "/view";
    }

    @GetMapping("/newPost")
    public String newPost(){
        return "newPost.html";
    }

    @PostMapping("/newPost")
    public String addNewPost(@ModelAttribute("formData") FormData formData){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserName = authentication.getName();
        String currentUserAuthorities = String.valueOf(authentication.getAuthorities());

        Post post = new Post();
        post.setTitle(formData.getTitle());
        post.setExcerpt(formData.getContent().substring(0,formData.getContent().length()/10)+"........ ");
        post.setContent(formData.getContent());
        if(currentUserAuthorities.equals("[ADMIN]")){
            post.setAuthor(formData.getAuthor());
        } else {
            post.setAuthor(currentUserName);
        }
        post.setPublishedAt(LocalDateTime.now());
        post.setPublished(true);
        post.setCreatedAt(LocalDateTime.now());
        post.setUpdatedAt(LocalDateTime.now());

        String [] tags = formData.getTags().toArray(new String[0]);//TODO
        for(String tag : tags){
            System.out.println(tag);
            if(!tag.trim().equals("")){
                List<Tag> sameTag =  tagService.findAllByNameIgnoreCase(tag);
                if(!sameTag.isEmpty()){
                    post.addTags(sameTag.get(0));
                } else {
                   Tag createNewTag = new Tag();
                   createNewTag.setName(tag);
                   createNewTag.setCreatedAt(LocalDateTime.now());
                   createNewTag.setUpdatedAt(LocalDateTime.now());
                   tagService.save(createNewTag);
                   List<Tag> savedTags = tagService.findAllByNameIgnoreCase(createNewTag.getName());
                   post.addTags(savedTags.get(0));
                }
            }
        }
        postService.save(post);
        return "redirect:/1";
    }

    @GetMapping("/edit/{id}")
    public String editPost(@PathVariable("id") Integer id,
                           Model model){
        Optional<Post> optionalPosts = postService.getPost(id);
        Post post = optionalPosts.get();
        FormData formData = new FormData();
        formData.setContent(post.getContent());
        formData.setId(post.getId());
        formData.setTitle(post.getTitle());
        formData.setTags(post.listOfTags());

        model.addAttribute("formData", formData);
        model.addAttribute("id", id);
        return "/edit";
    }

    @PostMapping("/edit")
    public String editPosts(@ModelAttribute("formData") FormData formData,
                            @RequestParam(name = "allTags", required = false)String allTags,
                            @ModelAttribute(name = "id") Integer id,
                            @RequestParam(name = "tagsToDelete", required = false)String tagsToDelete){
        Optional<Post> optionalPosts = postService.getPost(formData.getId());
        Post post = optionalPosts.get();
        post.setTitle(formData.getTitle());
        post.setExcerpt(formData.getContent().substring(0,formData.getContent().length()/10)+"........ ");
        post.setContent(formData.getContent());
        post.setUpdatedAt(LocalDateTime.now());

        if(tagsToDelete != null){
            String [] arrayOfTagsToDelete = tagsToDelete.trim().split(",");
            List<Tag> allTagsOfPost = post.getTags();
            for(String tagDelete : arrayOfTagsToDelete){
                for(Tag tag : allTagsOfPost){
                    if(tag.getName().trim().equalsIgnoreCase(tagDelete.toUpperCase())){
                        allTagsOfPost.remove(tag);
                        break;
                    }
                }
            }
        }

        String [] tags = allTags.trim().split(",");
        for(String tag : tags){
            if(!tag.trim().equals("")){
                List<Tag> sameTag =  tagService.findAllByNameIgnoreCase(tag);
                if(!sameTag.isEmpty()){
                    post.addTags(sameTag.get(0));
                } else {
                    Tag createNewTag = new Tag();
                    createNewTag.setName(tag);
                    createNewTag.setCreatedAt(LocalDateTime.now());
                    createNewTag.setUpdatedAt(LocalDateTime.now());
                    tagService.save(createNewTag);
                    List<Tag> savedTags = tagService.findAllByNameIgnoreCase(createNewTag.getName());
                    post.addTags(savedTags.get(0));
                }
            }
        }
        postService.save(post);
        return "redirect:/1";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        postService.deleteById(id);
        return "redirect:/1";
    }

    @RequestMapping("/register")
    public String showRegistrationForm(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "signup_form.html";
    }

    @PostMapping("/process_register")
    public String saveUser(@RequestParam("authority")String authority,
                           @ModelAttribute("user")User user){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        userDetailsService.save(user, authority);
        return "register-success.html";
    }
}
