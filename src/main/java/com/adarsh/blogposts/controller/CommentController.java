package com.adarsh.blogposts.controller;

import com.adarsh.blogposts.entity.Comment;
import com.adarsh.blogposts.entity.Post;
import com.adarsh.blogposts.entity.User;
import com.adarsh.blogposts.service.CommentService;
import com.adarsh.blogposts.service.PostService;
import com.adarsh.blogposts.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private PostService postService;

    @Autowired
    private UserDetailsService userDetailsService;

    public CommentController() {

    }
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    public CommentController(PostService postService){
        this.postService = postService;
    }

    @PostMapping("/newComment/{id}")
    public String saveComment(@PathVariable("id") Integer id,
                              @ModelAttribute("commentFormData") Comment commentFormData,
                              Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserName = authentication.getName();
        String currentUserAuthorities = String.valueOf(authentication.getAuthorities());
        User user = userDetailsService.findByName(currentUserName);

        model.addAttribute("id", id);
        Optional<Post> optionalPost = postService.getPost(commentFormData.getId());
        Post post = optionalPost.get();
        if(!commentFormData.getComment().trim().equals("")){
            Comment comment = new Comment();
            comment.setComment(commentFormData.getComment());
            comment.setEmail(user.getEmail());
            comment.setName(currentUserName);
            comment.setCreatedAt(LocalDateTime.now());
            comment.setUpdatedAt(LocalDateTime.now());
            comment.setPostId(post);
            commentService.saveComment(comment);
        }
        return "redirect:/view/{id}";
    }

    @GetMapping("/editcomment/{id}")
    public String editComment(@PathVariable("id") Integer id, Model model){
        Comment comment = commentService.getCommentById(id);
        model.addAttribute("obj", comment);
        return "editcomment.html";
    }

    @PostMapping("/editcomment/{id}")
    public String commentEdit(@ModelAttribute("commentFormData") Comment commentFormData, Model model){
        Comment comment = new Comment();
        if(!commentFormData.getComment().trim().equals("")){
            comment.setId(commentFormData.getId());
            comment.setName(commentFormData.getName());
            comment.setComment(commentFormData.getComment());
            comment.setUpdatedAt(LocalDateTime.now());
            comment.setCreatedAt(commentFormData.getCreatedAt());
            comment.setEmail(commentFormData.getEmail());
            comment.setPostId(commentFormData.getPostId());
            commentService.saveComment(comment);
        }
        return "redirect:/1";
    }

    @GetMapping("deletecomment/{id}")
    public String deleteComment(@PathVariable("id")Integer id,Model model){
        Comment comment = commentService.getCommentById(id);
        commentService.deleteComment(comment);
        return "redirect:/1";
    }
}
